import Vue from 'vue'
import App from './App.vue'
import { store } from './store'
import { routes } from './routes'


// styles's import
import 'swiper/dist/css/swiper.min.css'
import './assets/scss/main.scss'



// packages's imports
// font awesome
import { library as fontAwesomeLib } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import {
    faPhone,
    faAngleLeft,
    faAngleRight,
    faAngleDoubleDown,
    faPlus,
    faMinus,
    faCheck,
    faTimes,
    faEnvelope,
    faMapMarkerAlt,
    faCaretRight
} from '@fortawesome/free-solid-svg-icons'
import {
    faFacebookF,
    faTwitter,
    faVk,
    faLinkedinIn
} from '@fortawesome/free-brands-svg-icons'
// swiper
import Swiper from 'vue-awesome-swiper'
// vue router
import VueRouter from 'vue-router'
// vue resource
import VueResource from 'vue-resource'
// vuelidate
import Vuelidate from 'vuelidate'
// vue mask
import VueMask from 'v-mask'



// font awesome init function
fontAwesomeLib.add(
    faPhone,
    faAngleLeft,
    faAngleRight,
    faAngleDoubleDown,
    faPlus,
    faMinus,
    faCheck,
    faTimes,
    faEnvelope,
    faMapMarkerAlt,
    faCaretRight,
    faFacebookF,
    faTwitter,
    faVk,
    faLinkedinIn
);
Vue.component('fa-icon', FontAwesomeIcon);



// vue use functions
Vue.use(Swiper);
Vue.use(VueRouter);
Vue.use(VueResource);
Vue.use(Vuelidate);
Vue.use(VueMask);


// router init function
let router = new VueRouter(routes);
router.afterEach(() => {
    window.scroll({
        top: 0,
        left: 0,
        behavior: 'smooth'
    })
});



Vue.config.productionTip = false;

new Vue({
    store,
    router,
    render: h => h(App),
}).$mount('#app');
