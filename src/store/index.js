import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        pizzas: [],
        snacks: [],
        drinks: [],
        cart: [],
        totalCartPrice: 0,
        isEmptyCart: true,
        activeRoute: ''
    },
    getters: {
        pizzas(state) {
            return state.pizzas;
        },
        snacks(state) {
            return state.snacks;
        },
        drinks(state) {
            return state.drinks;
        },
        saleItems: (state) => {
            return (string) => {
                let saleItems = [];
                state[string].forEach((el) => {
                    for (let key in el) {
                        if (key.toString().indexOf('newPrice') === 0 && el[key]) {
                            saleItems.push(el);
                            return;
                        }
                    }
                });
                if (!saleItems.length) return state[string];
                return saleItems;
            }
        },
        cart(state) {
            return state.cart;
        },
        item: (state) => {
            return (id, route) => {
                let item;
                state[route].find((el) => {
                    if (el.id === id) item = el;
                });
                return {...item};
            }
        },
        isEmptyCart(state) {
            return state.isEmptyCart;
        },
        totalCartPrice(state) {
            return state.totalCartPrice;
        },
        activeRoute(state) {
            return state.activeRoute;
        }
    },
    mutations: {
        setProductToCart(state, payload) {
            let addedItem = this.getters.item(payload.id, state.activeRoute),
                sameCartItem = false;

            addedItem.quantity = payload.quantity;
            addedItem.size = payload.size;
            addedItem.type = payload.type;

            if (state.cart.length) {
                state.cart.forEach((item) => {
                    if (
                        item.id === payload.id &&
                        item.size === payload.size &&
                        item.type === payload.type
                    ) {
                        item.quantity += payload.quantity;
                        sameCartItem = true;
                    }
                });
            }
            if (!sameCartItem) {
                state.cart.push({...addedItem});
                this.commit('changeCartState', false);
            }
            this.commit('countTotalCartPrice');
        },
        changeItemQuantity(state, payload) {
            if (payload.value) {
                state.cart[payload.index].quantity++;
            } else {
                state.cart[payload.index].quantity--;
                if (state.cart[payload.index].quantity === 0) {
                    this.commit('removeItemFromCart', payload.index);
                }
            }
            this.commit('countTotalCartPrice');
        },
        countTotalCartPrice(state) {
            let totalCartPrice = 0;
            if (state.cart.length) {
                state.cart.forEach((el) => {
                    if (el[`newPrice${el.size}`]) totalCartPrice += el[`newPrice${el.size}`] * el.quantity;
                    else totalCartPrice += el[`price${el.size}`] * el.quantity;
                });
            }
            state.totalCartPrice = totalCartPrice;
            if (!totalCartPrice) this.commit('changeCartState', true);
        },
        changeCartState(state, val) {
            state.isEmptyCart = val;
        },
        removeItemFromCart(state, index) {
            state.cart.splice(index, 1);
            this.commit('countTotalCartPrice');
        },
        removeAllItemsFromCart(state) {
            state.cart = [];
            this.commit('countTotalCartPrice');
        },
        setActiveRoute(state, string) {
            state.activeRoute = string;
        },
        setItems(state, payload) {
            let productRoutes = [];
            for (let key in payload) productRoutes.push(key);
            productRoutes.forEach((item) => {
                payload[item].forEach((el) => {
                    let priceCounter = 0;
                    for (let prop in el) {
                        if (prop.indexOf('price') === 0) priceCounter++;
                    }
                    el.sizes = priceCounter;
                });
                state[item] = payload[item]
            });
        }
    }
});
