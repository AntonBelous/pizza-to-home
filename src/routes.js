import Home from './pages/Home'
import Snacks from './pages/Snacks'
import Drinks from './pages/Drinks'
import Product from './pages/Product'

export const routes = {
    mode: 'history',
    routes: [
        {
            path: '/',
            component: Home
        },
        {
            path: '/pizzas/:id',
            component: Product
        },
        {
            path: '/snacks',
            component: Snacks
        },
        {
            path: '/snacks/:id',
            component: Product
        },
        {
            path: '/drinks',
            component: Drinks
        },
        {
            path: '/drinks/:id',
            component: Product
        },
    ]
};

